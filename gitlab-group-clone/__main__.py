from gql import gql, Client
from gql.transport.aiohttp import AIOHTTPTransport
import pygit2
import sys
import getpass

token = "glpat-ffyBsyDi8crryMLRcNcj"

transport = AIOHTTPTransport(url="https://gitlab.com/api/graphql", headers={'Authorization': f"Bearer {token}"})

client = Client(transport=transport, fetch_schema_from_transport=True)

def query_groups(client):
    query = gql(
            """{
            currentUser {
               groups {
                  nodes {
                    name
                    fullPath
                  }
                }
              }
            }
    """
    )
    return client.execute(query)["currentUser"]["groups"]["nodes"]

def query_group_projects(client, group):
    group_projects_query = gql(
            """
            query Query($group: ID!) {
              group(fullPath: $group) {
                projects {
                  nodes {
                    fullPath
                    sshUrlToRepo
                  }
                }
              }
            }
    """
    )
    return client.execute(group_projects_query, {"group": group})["group"]["projects"]["nodes"]

if sys.argv[1] == "clone":
    group = sys.argv[2]
    projects = query_group_projects(client, group)
    keypair = pygit2.KeypairFromAgent("git")
    callbacks = pygit2.RemoteCallbacks(credentials=keypair)
    for project in projects:
        clone_url = project["sshUrlToRepo"]
        repo_path = project["fullPath"]
        pygit2.clone_repository(clone_url, repo_path, callbacks=callbacks)
